%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(nn).

%% nn: 

-export([ dot/1 ]).

%% API

dot (Filename) ->
    Cortex = spawn(fun () -> nn_cell:cortex(Filename) end),
    Cortex ! {self(), start},
    Cortex.

%% Internals

%% End of Module.
