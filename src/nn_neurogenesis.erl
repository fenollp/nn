%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(nn_neurogenesis).

%% nn_neurogenesis: where the NN is read|done|undone.

-export([ from_file/1
        ,   to_file/2 ]).

%% API

from_file (Filename) ->
    {ok, Dot} = dot:from_file(Filename),
    {ok, DG} = dot:load_graph(Dot),
    Cells = from_genotype(DG),
    {Sensors, Neurons, Actuators} = unzip_cells(Cells, [], [], []),
    io:format("Cortex ~p's...\n"
              "\t""sensors: ~p\n"
              "\t""neurons: ~p\n"
              "\t""actuators: ~p\n",
              [self(),Sensors,Neurons,Actuators]),
    {ok, DG, Sensors, Neurons, Actuators}.

to_file (Filename, G) ->
    Dot = {dot,digraph,false,<<"nn">>,
           lists:filtermap(
             fun (V) ->
                     case digraph:vertex(G, V) of
                         {V, []} ->
                             false;
                         {V, Label} ->
                             {true, {node,{nodeid,V,<<>>,<<>>},
                                     [ {'=', Ke, str(Va)}
                                       || {Ke,Va} <- Label, not is_atom(Ke) ]
                                    }
                             }
                     end
             end, digraph:vertices(G))
           ++
           [ begin
                 {_, A, B, _Label} = digraph:edge(G, E),
                 {'->'
                 ,{nodeid,A,<<>>,<<>>}
                 ,{nodeid,B,<<>>,<<>>},[]}
             end || E <- digraph:edges(G) ]},
    ok = dot:to_file(Filename, Dot),
    io:format("Saved nn to '~s'\n", [Filename]).

%% Internals

str (Value) ->
    hd(io_lib:format("~p", [Value])).

from_genotype (DG) ->
    Self = self(),
    nn_digraph:foreach( %% Spawn the neurons
      fun ({Vertex, _Out}) ->
              DotLabel = nn_digraph:label(DG, Vertex),
              KVs = lists:keymap(fun extract_Erlang_KVs/1, 2, DotLabel),
              {_, UUID} = lists:keyfind(<<"uuid">>, 1, KVs),
              case UUID of
                  [$a|_] ->
                      Type = actuator,
                      Pid = spawn(fun () -> nn_cell:actuator(Self, KVs) end);
                  [$s|_] ->
                      Type = sensor,
                      Pid = spawn(fun () -> nn_cell:sensor(Self, KVs) end);
                  ______ ->
                      Type = neuron,
                      Pid = spawn(fun () -> nn_cell:neuron(Self, KVs) end)
              end,
              nn_digraph:label(DG, Vertex, [{pid,Pid},{type,Type}] ++ KVs)
      end, DG),
    nn_digraph:map( %% Match UUIDs to PIDs and Weights. Return PIDs.
      fun ({Vertex, _}) ->
              Label = nn_digraph:label(DG, Vertex),
              {_, Type} = lists:keyfind(type, 1, Label),
              {_, PID} = lists:keyfind(pid, 1, Label),
              case Type of
                  neuron ->
                      UUIDWeights = proplists:get_value(<<"weights">>, Label, []),
                      PIDWeights = weight_inputs(UUIDWeights, DG, Vertex),
                      Os = neighbourgs(DG, Vertex, fun digraph:out_neighbours/2),
                      PID ! {self(), init, PIDWeights, Os};
                  sensor ->
                      Os = neighbourgs(DG, Vertex, fun digraph:out_neighbours/2),
                      PID ! {self(), init, Os};
                  actuator ->
                      Is = neighbourgs(DG, Vertex, fun digraph:in_neighbours/2),
                      PID ! {self(), init, Is}
              end,
              {Type, PID} %% Given to unzip_cells/4 later on.
      end, DG).

unzip_cells ([{Cell,Pid}|Rest], Sensors, Neurons, Actuators) ->
    case Cell of
        sensor   -> unzip_cells(Rest, [Pid|Sensors], Neurons, Actuators);
        neuron   -> unzip_cells(Rest, Sensors, [Pid|Neurons], Actuators);
        actuator -> unzip_cells(Rest, Sensors, Neurons, [Pid|Actuators])
    end;
unzip_cells ([], Sensors, Neurons, Actuators) ->
    {Sensors, Neurons, Actuators}.

weight_inputs (UUIDWeights, G, V) ->
    [ begin
          Label = nn_digraph:label(G, I),
          {_, PID}  = lists:keyfind(pid, 1, Label),
          {_, UUID} = lists:keyfind(<<"uuid">>, 1, Label),
          {_, WeightL} = lists:keyfind(UUID, 1, UUIDWeights),
          {PID, WeightL} %% Switch to PID because it's more Erlang
      end || I <- digraph:in_neighbours(G, V) ].

neighbourgs (G, N, Neighbourgs) ->
    [ begin
          Label = nn_digraph:label(G, V),
          {pid, PID} = lists:keyfind(pid, 1, Label),
          PID
      end || V <- Neighbourgs(G, N) ].

extract_Erlang_KVs (Bin) when is_binary(Bin) ->
    Str = binary_to_list(Bin),
    {ok, Scanned, _} = erl_scan:string(Str ++ "."),
    {ok, Parsed} = erl_parse:parse_exprs(Scanned),
    Bindings = erl_eval:new_bindings(),
    {value, KVs, _NewBindings} = erl_eval:exprs(Parsed, Bindings),
    KVs.

%% End of Module.
