%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(nn_digraph).

%% nn_digraph: An API to stdlib's ETS-based digraph.

-export([        new/0
        , add_vertex/2
        ,   add_edge/3
        ,    foreach/2
        ,        map/2
        ,  filtermap/2
        ,      label/2
        ,      label/3
        ,     target/3 ]).

-define(EDGE_T, ['$e'|N] when is_integer(N)).

%% API

new () ->
    digraph:new([ cyclic
                , protected ]).

add_vertex (G, V) ->
    digraph:add_vertex(G, V).

add_edge (G, Va, Vb) ->
    digraph:add_edge(G, Va, Vb).

label (G, X) ->
    {X, Label} = case X of
                     ?EDGE_T ->
                         digraph:edge(G, X);
                     _Vertex ->
                         digraph:vertex(G, X)
                 end,
    Label.

label (G, X, Label) ->
    case X of
        ?EDGE_T ->
            digraph:add_edge(G, X, Label);
        _Vertex ->
            digraph:add_vertex(G, X, Label)
    end.

foreach (Fun, Graph) ->
    run_through(fun lists:foreach/2, Fun, Graph).

map (Fun, Graph) ->
    run_through(fun lists:map/2, Fun, Graph).

filtermap (Fun, Graph) ->
    run_through(fun lists:filtermap/2, Fun, Graph).

target (G, VSource, Edge) ->
    {Edge, VSource, VTarget, _Label} = digraph:edge(G, Edge),
    VTarget.

%% Internals

run_through (How, Fun, Graph) ->
    Vertices = digraph:vertices(Graph),
    How(fun (Vertex) ->
                Out = digraph:out_edges(Graph, Vertex),
                Fun({Vertex, Out})
        end, Vertices).

%% End of Module.
