%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(nn_cell).

%% nn_cell: where the different phenotypic cells are made.

-export([   neuron/2
        ,   sensor/2
        , actuator/2
        ,   cortex/1 ]).

%% API

neuron (_Parent, KVs) ->
    io:format("~p created ~p (Neuron ~p)\t~p\n",
              [_Parent,self(),proplists:get_value(<<"uuid">>,KVs),KVs]),%%
    Bias = proplists:get_value(<<"bias">>, KVs, 1.0),
    ActivationF = proplists:get_value(<<"af">>, KVs, fun math:tanh/1),%fun (X) -> X end),%
    init_neuron(ActivationF, Bias).

sensor (_Parent, KVs) ->
    io:format("~p created ~p (Sensor ~p)\t~p\n",
              [_Parent,self(),proplists:get_value(<<"uuid">>,KVs),KVs]),%%
    Fun = proplists:get_value(
            <<"fun">>, KVs,
            fun () -> io:format("~p gets.\n", [self()]), [1] end),
    init_sensor(Fun).

actuator (Parent, KVs) ->
    io:format("~p created ~p (Actuator ~p)\t~p\n",
              [Parent,self(),proplists:get_value(<<"uuid">>,KVs),KVs]),%%
    Fun = proplists:get_value(
            <<"fun">>, KVs,
            fun (Nx) -> io:format("~p puts ~p.\n", [self(),Nx]) end),
    init_actuator(Parent, Fun).

cortex (Filename) ->
    io:format("Cortex ~p created using '~s'\n", [self(),Filename]),%%
    init_cortex(Filename).

%% Internals

-define( MSG_pass(Pid, Data)
       , {Pid, pass, Data} ).

-define( MSG_sync(PID)
       , {PID, sync} ).

-define( handle_stopping(Name)
       , {_From, stop} ->
               io:format(Name " ~p stopping.\n", [self()]) ).

init_neuron (ActivationF, Bias) ->
    receive
        {_From, init, Weights, Os} ->
            neuron_(Weights, Os, 0, 0, length(Weights), ActivationF, Bias)
    end.

%% Weights :: [{Pid,WeightL}]. Weights of inputs.
%% Os :: [pid()]. PIDs of outputs
%% Sum = dot.prod of WeightL and Incoming
%% Ticks = len ToSum < Limit. Incr for each incoming.
%% Limit = len Weights. Used when sync needed. = 1 <> dynamic.

neuron_ (Weights, Os, Sum, Ticks, Limit, AF, Bias) ->
    receive
        ?MSG_pass(PIDx, Nx) ->
            io:format("~p (N) has ~p/~p: ~p\n",[self(),Ticks+1,Limit,Nx]),%%
            {_, WeightL} = lists:keyfind(PIDx, 1, Weights),
            Prod = dot(Nx, WeightL) + Sum,
            case Ticks + 1 of
                Limit ->
                    [O ! ?MSG_pass(self(), [AF(Bias+Prod)]) || O <- Os],
                    neuron_(Weights, Os, 0, 0, Limit, AF, Bias);
                _ ->
                    neuron_(Weights, Os, Prod, Ticks + 1, Limit, AF, Bias)
            end;

        ?handle_stopping("Neuron")
    end.

dot ([N|Ns], [W|Ws]) ->
    N * W + dot(Ns, Ws);
dot ([], []) ->
    0.

rand () ->
    random:uniform() - 0.5.


init_sensor (Fun) ->
    receive
        {_From, init, Os} ->
            sensor_(Fun, Os)
    end.

sensor_ (Get, Os) ->
    receive
        ?MSG_sync(_From) ->
            Vector = Get(),
            io:format("~p (S) sends ~p to ~p\n", [self(), Vector, Os]),%%
            _ = [O ! ?MSG_pass(self(), Vector) || O <- Os],
            sensor_(Get, Os);

        ?handle_stopping("Sensor")
    end.


init_actuator (Cortex, Fun) ->
    receive
        {_From, init, Is} ->
            actuator_(Cortex, Fun, Is)
    end.

actuator_ (Cortex, Put, Is) ->
    receive
        ?MSG_pass(PIDx, Nx) ->
            %true = lists:any(fun (X) -> PIDx =:= X end, Is), %% Security
            Put(Nx),
            io:format("~p (A) receives ~p from ~p\n", [self(), Nx, PIDx]),%%
            Cortex ! ?MSG_sync(self()),
            actuator_(Cortex, Put, Is);

        ?handle_stopping("Actuator")
    end.


init_cortex (Filename) ->
    {ok, Digraph, Sensors, Neurons, Actuators} =
        nn_neurogenesis:from_file(Filename),
    cortex_(Digraph, Sensors, Neurons, Actuators, 0, length(Actuators), 0).

cortex_ (G, Ss, Ns, As, Ticks, Limit, Spin) ->
    receive
        {_From, export, Filename} ->
            ok = nn_neurogenesis:to_file(Filename, G),
            cortex_(G, Ss, Ns, As, Ticks, Limit, Spin);

        {From, digraph} ->
            From ! {digraph, G},
            cortex_(G, Ss, Ns, As, Ticks, Limit, Spin);

        {_From, start} ->
            _ = [S ! ?MSG_sync(self()) || S <- Ss],
            cortex_(G, Ss, Ns, As, Ticks, Limit, Spin);

        ?MSG_sync(From) ->
            %TODO: a way to break the loop (priority receive + Ticks(A) ?)
            io:format("~p (C) spin~p sees ~p/~p from ~p\n", [self(),Spin+1,Ticks+1,Limit,From]),%%
            case Ticks + 1 of
                Limit ->
                    %%_ = [S ! ?MSG_sync(self()) || S <- Ss],
                    cortex_(G, Ss, Ns, As, 0, Limit, Spin + 1);
                _ ->
                    cortex_(G, Ss, Ns, As, Ticks + 1, Limit, Spin)
            end;

        ?handle_stopping("Cortex"),
            _ = [Pid ! {self(), stop} || Pid <- Ss],
            _ = [Pid ! {self(), stop} || Pid <- Ns],
            _ = [Pid ! {self(), stop} || Pid <- As],
            ok
    end.

%% End of Module.
