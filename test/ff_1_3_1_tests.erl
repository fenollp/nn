%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(ff_1_3_1_tests).

%% ff_1_3_1_tests: testing a simple Feed Forward NN.

-include_lib("eunit/include/eunit.hrl").


%% API tests.

do_test () ->
    true = register(main, self()),
    Cortex = spawn(fun () -> nn_cell:cortex("test/ff_1_3_1.dot") end),
    Cortex ! {self(), start},
    [R] = receive {result, N} -> N end,
    %% Cortex ! {self(), stop},
    true = unregister(main),
    ?assertEqual(math:tanh(3 * math:tanh(math:tanh(1 +1) +1) +1), R).

export_test () ->
    true = register(main, self()),
    Cortex = spawn(fun () -> nn_cell:cortex("test/ff_1_3_1.dot") end),
    Cortex ! {self(), start},
    [_R] = receive {result, N} -> N end,
    true = unregister(main),
    Tmp = "test/ff_1_3_1.dot_",
    Cortex ! {self(), export, Tmp},
    timer:sleep(200),
    {ok, Bin} = file:read_file(Tmp),
    ok = file:delete(Tmp),
    ?assertEqual(<<"digraph nn {\n"
                   "\t5 [uuid=\"5\", weights=\"[{2,[1]},{3,[1]},{4,[1]}]\"] ;\n"
                   "\ta1 [uuid=\"a1\", fun=\"#Fun<erl_eval.6.80484245>\"] ;\n"
                   "\t3 [uuid=\"3\", weights=\"[{1,[1]}]\"] ;\n"
                   "\ts1 [uuid=\"s1\", fun=\"#Fun<erl_eval.20.80484245>\"] ;\n"
                   "\t1 [uuid=\"1\", weights=\"[{\"s1\",[1]}]\"] ;\n"
                   "\t4 [uuid=\"4\", weights=\"[{1,[1]}]\"] ;\n"
                   "\t2 [uuid=\"2\", weights=\"[{1,[1]}]\"] ;\n"
                   "\t2 -> 5;\n"
                   "\t1 -> 4;\n"
                   "\t1 -> 3;\n"
                   "\t4 -> 5;\n"
                   "\t5 -> a1;\n"
                   "\t1 -> 2;\n"
                   "\ts1 -> 1;\n"
                   "\t3 -> 5;\n"
                   "}\n">>, Bin).

%% End of Module.
